package VO;

import model.data_structures.LinkedList
;
import model.data_structures.Lista;
import VO.Servicio;

/**
 * VO utilizado en Req 5A, tiene el rango de distancia y la lista de servicios cuya distancia recorrida 
 * pertenece a dicho rango
 */
public class RangoDistancia implements Comparable<RangoDistancia>
{
	//ATRIBUTOS
	
    /**
     * Modela el valor minimo del rango
     */
	private double limiteSuperior;
	
	/**
	 * Modela el valor m�ximo del rango
	 */
	private double limineInferior;
	
	/**
	 * Modela la lista de servicios cuya distancia recorrida esta entre el l�mite inferior y el l�mite superior
	 */
	private Lista<Service> serviciosEnRango;

	//M�TODOS
	
	public RangoDistancia(double pLimiteSuperior, double pLimiteInferior)
	{
		limiteSuperior = pLimiteSuperior;
		limineInferior = pLimiteInferior ;
		serviciosEnRango = new Lista<Service>();
		
	}

	/**
	 * @return the limiteSuperior
	 */
	public double getLimiteSuperior()
	{
		return limiteSuperior;
	}

	/**
	 * @return the limineInferior
	 */
	public double getLimineInferior() 
	{
		return limineInferior;
	}

	/**
	 * @return the serviciosEnRango
	 */
	public Lista<Service> getServiciosEnRango() 
	{
		return serviciosEnRango;
	}

	/**
	 * @param serviciosEnRango the serviciosEnRango to set
	 */

	@Override
	public int compareTo(RangoDistancia o)
	{
		int vari = 0 ;
		if(limiteSuperior > o.getLimiteSuperior())
		{
			vari = 1 ;
		}
		else if(limiteSuperior < o.getLimiteSuperior())
		{
			vari = -1 ;
		}		
		
		return vari;
	}
}
