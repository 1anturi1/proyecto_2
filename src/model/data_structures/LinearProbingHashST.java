package model.data_structures;

import model.data_structures.*;


public class LinearProbingHashST <Key, Value>
{
	private int N; 

	private int M ; 

	private Object[] keys; 

	private Object[] vals; 

	public LinearProbingHashST(int tamanio)
	{
		M = tamanio;
		N = 0;
		keys =  new Object[M];
		vals =  new Object[M];
	}
	public boolean isEmpty()
	{
		return size()==0;
	}
	public int size()
	{
		return N;
	}

	private int hash(Key key)
	{
		return (key.hashCode() & 0x7fffffff) % M;
	}

	public void put(Key key, Value val)
	{
		if (N >= M/2)
			resize(2*M); 

		int i;

		for (i = hash(key); keys[i] != null; i = (i + 1) % M)
		{	
//			System.out.println("key " + keys[i] + " " + i);
			if (keys[i].equals(key))
			{
				vals[i] = val;
				return;
			}
		}
		keys[i] = key;
		vals[i] = val;
		N++;
	}

	public Object get(Key key)
	{
		for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
			if (keys[i].equals(key))
				return vals[i];
		return null;
	}


	public void resize(int tamanio)
	{
		LinearProbingHashST<Key, Value> temp = new LinearProbingHashST<Key, Value>(tamanio);
		
		for(int i = 0; i < keys.length ; i++)
		{
			if(keys[i]!=null)
			temp.put((Key)keys[i], (Value)vals[i]);
		}
		keys = temp.keys;
		vals=temp.vals;
		M=temp.M;
	}
	
}
