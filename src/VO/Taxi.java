package VO;

import model.data_structures.Lista;

public class Taxi implements Comparable<Taxi> 
{
	private String taxiId;
	
	private String company;
	
	private Lista<Service> servicios;
    
	public Taxi(String pTaxiId, String pCompany)
	{
		taxiId=pTaxiId;
		company=pCompany;
		servicios= new Lista<Service>();
	}
	/**
     * @return id - taxi_id
     */
	public void agregarServicio(Service pServ)
	{
		servicios.add(pServ);
	}
	
	public Lista<Service> getServicios()
	{
		return servicios;
	}
	
    public String getTaxiId()
    {
        // TODO Auto-generated method stub
        return taxiId;
    }
    
    
    /**
     * @return company
     */
    public String getCompany()
    {
        // TODO Auto-generated method stub
        return company;
    }

    @Override
    public int compareTo(Taxi o)
    {
		int variable = 0;

		if(taxiId.compareTo(o.taxiId) > 0)
		{
			variable =1 ;
		}

		else if(taxiId.compareTo(o.taxiId) < 0)
		{
			variable = -1 ;
		}

		return variable ;
    }
}
