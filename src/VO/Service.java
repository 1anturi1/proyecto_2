package VO;

import java.awt.Point;
import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> 
{

	private String tripId;

	private String taxiId ;	

	private String fechaInicial ;
	
	private String horaInicial ;
	
	private String fechaFinal ;
	
	private String horaFinal ;

	private int tripSeconds ;

	private double tripMiles;

	private String pickupCensusTract;

	private String dropoffCensusTract;

	private int pickupCommunityArea;

	private int dropoffCommunityArea;

	private float fare;

	private float tips;

	private float tolls;

	private double extras;

	private float tripTotal ;

	private String paymentType;

	private String company;

	private double pickupCentroidLatitude;

	private double pickupCentroidLongitude;

	private Point pickupCentroidLocation;

	private double dropoffCentroidLatitude;	

	private double dropoffCentroidLongitude;	

	private Point dropoffCentroidLocation;




	public Service(String ptripId, String pTaxiId, String pFechaInicial, String pHoraInicial, String pFechaFinal, String pHoralfinal, 
			 int pTripSeconds, double ptripMiles, String pPickupCensusTrack, String pDropoffCensusTrack,
			 int pCommunity, int pDropoffCommunityArea, float pFare, float pTips, float pTolls, double pExtras ,
			 float pTripTotal, String pPaymentTipe, String pCompany, double pPickupCentroidLatitude, double pPickupCentroidLongitude,
			 Point pPickupCentroidLocation, double pDropoffCentroidLongitude, double pDropoffCentroidLatitude, Point pDropoffCentroidLocation)
	{
		tripId = ptripId;
		taxiId = pTaxiId ;
		fechaInicial =pFechaInicial ;
		horaInicial = pHoraInicial;
		fechaFinal = pFechaFinal ;
		horaFinal = pHoralfinal;
		tripSeconds = pTripSeconds ;
		tripMiles = ptripMiles ;
		pickupCensusTract = pPickupCensusTrack ;
		dropoffCensusTract = pDropoffCensusTrack ;
		pickupCommunityArea =pCommunity ;
		dropoffCommunityArea = pDropoffCommunityArea ;
		fare = pFare ;
		tips = pTips ;
		tolls= pTolls ;
		extras = pExtras ;
		tripTotal = pTripTotal ;
		paymentType = pPaymentTipe ;
		company = pCompany ;
		pickupCentroidLatitude = pPickupCentroidLatitude;
		pickupCentroidLongitude = pPickupCentroidLongitude ;
		pickupCentroidLocation = pPickupCentroidLocation ;
		dropoffCentroidLongitude = pDropoffCentroidLongitude;
		dropoffCentroidLocation = pDropoffCentroidLocation;

	}



	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		return tripId;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		
		return taxiId;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return tripTotal;
	}

	public int getCommunityArea()
	{
		return pickupCommunityArea ;
	}


	public String getFechaInicial() {
		return fechaInicial;
	}



	public String getHoraInicial() {
		return horaInicial;
	}



	public String getFechaFinal() {
		return fechaFinal;
	}



	public String getHoraFinal() {
		return horaFinal;
	}



	public String getPickupCensusTract()
	{
		return pickupCensusTract;
	}



	public String getDropoffCensusTract() 
	{
		return dropoffCensusTract;
	}



	public int getPickupCommunityArea() 
	{
		return pickupCommunityArea;
	}



	public int getDropoffCommunityArea() 
	{
		return dropoffCommunityArea;
	}



	public float getFare() 
	{
		return fare;
	}



	public float getTips() 
	{
		return tips;
	}



	public float getTolls()
	{
		return tolls;
	}



	public double getExtras() 
	{
		return extras;
	}



	public String getPaymentType() 
	{
		return paymentType;
	}



	public String getCompany() 
	{
		return company;
	}



	public double getPickupCentroidLatitude() 
	{
		return pickupCentroidLatitude;
	}



	public double getPickupCentroidLongitude() 
	{
		return pickupCentroidLongitude;
	}



	public Point getPickupCentroidLocation()
	{
		return pickupCentroidLocation;
	}



	public double getDropoffCentroidLatitude() 
	{
		return dropoffCentroidLatitude;
	}



	public double getDropoffCentroidLongitude() 
	{
		return dropoffCentroidLongitude;
	}



	public Point getDropoffCentroidLocation() 
	{
		return dropoffCentroidLocation;
	}



	@Override
	public int compareTo(Service o) 
	{
		int variable = 0;

		if(pickupCommunityArea == o.pickupCommunityArea)
			return variable ;

		else if(pickupCommunityArea > o.pickupCommunityArea)
		{
			variable = 1 ;
		}
		else if(pickupCommunityArea < o.pickupCommunityArea)
		{
			variable = -1 ;
		}

		return variable ;
	}





}