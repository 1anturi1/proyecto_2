package model.data_structures;

public class arbolRojoNegro <Key extends Comparable <Key>, Value>
{
	private final static boolean RED=true;

	private final static boolean BLACK=false;

	private Nodo Raiz;
	
	private int N ;

	public arbolRojoNegro()
	{
		Raiz = null ;
	}
	
	
	public boolean isRed(Nodo x)
	{
		if(x == null)
		{
			return false;
		}
		return x.color()==RED;
	}
	

	public int size(Nodo x)
	{
		return x.size() ;
	}

	
	public int size()
	{
		return Raiz.size();
	}

	
	public boolean isEmpty()
	{
		return Raiz==null;
	}

	
	public Value get(Key key)
	{
		if(key==null)
		{
			System.out.println("el argumento del get es nulo");
		}
		return get(Raiz,key);
	}
	
	public Nodo getRaiz()
	{
		return Raiz;
	}
	
	public Value get(Nodo x, Key key)
	{
		
		Value aux = null ;
		
		while(x != null)
		{
			if(key.compareTo((Key)x.getKey()) < 0)
			{
				x = x.getIzq();
			}
			else if(key.compareTo((Key) x.getKey()) > 0)
			{
				
				x = x.getDer();
				
			}
			else
			{
				return (Value) x.getVal();

						
			}
		}
	
		return aux;
	}
	
	
	public boolean contains(Key key)
	{
		return get(key)!=null;
	}
	
	
	
	public void put(Key key, Value val)
	{
		if(key==null)
		{
			System.out.println("Llave de put es nula");
			return;
		}
		if(val==null)
		{
			delete(key);
			return;
		}
		Raiz=put(Raiz, key, val);
		Raiz.CambiarColor(BLACK);
	}
	
	
	private Nodo put(Nodo h,Key key, Value val)
	{
		if(h==null)
		{
			return new Nodo(key, val, RED);
		}
		
		if(key.compareTo( ( Key ) h.getKey( ) ) < 0)
		{
			h.cambiarIzq( put ( h.getIzq( ),key , val ) );
		}
		
		else if(key.compareTo( ( Key ) h.getKey( ) ) > 0 )
		{
			h.cambiarDer ( put ( h.getDer( ),key , val ) );
		}
		
		else
		{
			h.setVal(val);
		}
		
		if(isRed(h.getDer()) && !isRed(h.getIzq()))
		{
			h=rotateLeft(h);
		}
		if(isRed(h.getIzq())&& isRed(h.getIzq().getIzq()))
		{
			h=rotateRight(h);
		}
		if(isRed(h.getIzq())&& isRed(h.getDer()))
		{
			flipColors(h);
		}
		N++ ;
		return h;
	}
	
	
	
	private Nodo rotateRight(Nodo h)
	{
		Nodo x = h.getIzq();
		h.cambiarIzq(x.getDer());
		x.cambiarDer(h);
		x.CambiarColor(x.getDer().color());
		x.getDer().CambiarColor(RED);
		return x;
	}
	
	
	
	private Nodo rotateLeft(Nodo h)
	{

		Nodo x = h.getDer();
		h.cambiarDer(x.getIzq());
		x.cambiarIzq(h);
		x.CambiarColor(x.getIzq().color());
		x.getIzq().CambiarColor(RED);

		return x;
	}
	
	
	
	private void flipColors(Nodo h)
	{
		h.CambiarColor(!h.color());
		h.getIzq().CambiarColor(!h.getIzq().color());
		h.getDer().CambiarColor(!h.getDer().color());
	}
	
	
	
	public void deleteMin()
	{
		if(isEmpty())
		{
			System.out.println("desborado");
			return;
		}
		if((!isRed(Raiz.getIzq()))&& !isRed(Raiz.getDer()))
		{
			Raiz.CambiarColor(RED);
			Raiz=deleteMin(Raiz);
		}
		if(!isEmpty())
		{
			Raiz.CambiarColor(BLACK);
		}
	}
	
	
	
	private Nodo deleteMin(Nodo h)
	{
		if(h.getIzq()==null)
		{
			return null;
		}
		if(!isRed(h.getIzq())&& !isRed(h.getIzq().getIzq()))
		{
			h=moveRedLeft(h);
		}
		h.cambiarIzq(deleteMin(h.getIzq()));

		return balance(h);
	}
	
	
	
	public void deleteMax()
	{
		if(isEmpty())
		{
			System.out.println("desbordamiento");
			return;
		}
		if(!isRed(Raiz.getIzq())&& ! isRed(Raiz.getDer()))
		{
			Raiz.CambiarColor(RED);
		}
		Raiz = deleteMax(Raiz);
		if(!isEmpty() )
		{
			Raiz.CambiarColor(BLACK);
		}
	}
	
	
	private Nodo deleteMax(Nodo h)
	{
		if(isRed(h.getIzq()))
		{
			h=rotateRight(h);
		}
		if(h.getDer()==null)
		{
			return null;
		}
		if(!isRed(h.getDer()) && !isRed(h.getDer().getIzq()))
		{
			h= moveRedRight(h);
		}
		h.cambiarDer(deleteMax(h.getDer()));
		return balance(h);
	}
	
	
	public void delete(Key key)
	{
		if(key==null)
		{
			System.out.println("argumento de delete es nulo");
			return;
		}
		if(!contains(key))
		{
			return;
		}
		if(isRed(Raiz.getIzq())&& !isRed(Raiz.getDer()))
		{
			Raiz.CambiarColor(RED);

			Raiz = delete(Raiz, key); 
		}
		if(!isEmpty())
		{
			Raiz.CambiarColor(BLACK);
		}
	}
	
	
	
	private Nodo delete(Nodo h, Key key)
	{
		if(key.compareTo(key)<0)
		{
			h= moveRedLeft(h);
			h.cambiarIzq(delete(h.getIzq(), key));
		}
		else
		{
			if(isRed(h.getIzq()))
			{
				h=rotateRight(h);
			}
			if(key.compareTo((Key)h.getKey())==0 && (h.getDer()==null))
			{
				return null;
			}
			if(!isRed(h.getDer()) && !isRed(h.getDer().getIzq()))
			{
				h=moveRedRight(h);
			}
			if(key.compareTo((Key)h.getKey())==0)
			{
				Nodo x= min(h.getDer());
				h.setKey(x.getKey());
				h.setVal(x.getVal());
			}
			else
				h.cambiarDer(deleteMax(h.getDer()));
		}
		return balance(h);
	}
	
	
	private Nodo min(Nodo der) 
	{		
		return der.getIzq();
	}
	
	
	private Nodo moveRedLeft(Nodo h)
	{
		flipColors(h);
		if(isRed(h.getDer().getIzq()))
		{
			h.cambiarDer(rotateRight(h.getDer()));
			h=rotateRight(h);
			flipColors(h);
		}
		return h;
	}
	private Nodo moveRedRight(Nodo h)
	{
		flipColors(h);
		if(isRed(h.getIzq().getIzq()))
		{
			h=rotateRight(h);
			flipColors(h);
		}
		return h;
	}
	private Nodo balance(Nodo h)
	{
		if(isRed(h.getDer()))
		{
			h=rotateLeft(h);
		}
		if(isRed(h.getIzq())&& isRed(h.getIzq().getIzq()))
		{
			h=rotateRight(h);
		}
		if(isRed(h.getIzq())&& isRed(h.getDer()))
		{
			flipColors(h);
		}
		return h;
	}
}
