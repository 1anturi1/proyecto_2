package VO;

import model.data_structures.LinkedList;

public class ServiciosValorPagado 
{
	
	private LinkedList<Service> serviciosAsociados;
	private double valorAcumulado;
	public LinkedList<Service> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(LinkedList<Service> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	
	

}
