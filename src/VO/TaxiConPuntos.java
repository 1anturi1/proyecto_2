package VO;

import model.data_structures.Lista;

public class TaxiConPuntos implements Comparable<TaxiConPuntos> 
{
	private Lista<Service> service;
	
	private String taxiId;
	
	private String compania;
	
	public TaxiConPuntos(String pTaxiId, String pCompany) 
	{
		compania=pCompany;
		taxiId= pTaxiId;
		service= new Lista<Service>();
		// TODO Auto-generated constructor stub
	}

	/**
     * @return puntos - puntos de un Taxi
     */
	public void agregar(Service add)
	{
		service.add(add);
	}
	public Service get(Service a)
	{
		return service.get(a);
	}
	
	public Lista<Service> getseriveces()
	{
		return service;
	}
	
	public float getPuntos()
	{
		service.listing();
		float millas=0;
		float dinero=0;
		while(true)
		{
			Service proc= service.getCurrent();
			if(proc.getTripMiles()>0)
			{
				if(proc.getTripTotal()>0)
				{
					dinero+=proc.getTripTotal();
					millas+=proc.getTripMiles();
				}
			}
			if(service.next())
			{
				service.avanzar();
			}
			else
			{
				break;
			}
		}
		return (dinero/millas);
	}
	public String getId()
	{
		return taxiId;
	}
	
	@Override
	public int compareTo(TaxiConPuntos arg0) 
	{
		int var =0;
		if(arg0.taxiId.compareTo(taxiId)<0)
		{
			var=-1;
		}
		if(arg0.taxiId.compareTo(taxiId)>0)
		{
			var=1;
		}
		
		return var;
	}
}
