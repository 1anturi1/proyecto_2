package model.data_structures;

public class SeparateChainingHash <Key extends Comparable <Key>, Value>
{
	public final static int FactorDeCarga = 3600 ;
	
	private int M ; 
	public Node[] st ; 
	private int N ;

	public SeparateChainingHash()
	{
		N = 0 ;
		M = 74 ;
		st = new Node[M] ;
		
	}
	
	public int hash(Key key) 
	{
		return Math.abs((key.hashCode() & 0x7fffffff) % M); 
	}

	public static class Node 
	{
		private Object key ;
		private Object value ;
		private Node next ;

		public Node(Object pKey, Object pValue)
		{
			key = pKey ;
			value = pValue ;
		}
		
		public Object getKey() 
		{
			return key;
		}

		public void setKey(Object key) 
		{
			this.key = key;
		}

		public Object getValue() 
		{
			return value;
		}

		public void setValue(Object value) 
		{
			this.value = value;
		}

		public Node getNext() 
		{
			return next;
		}

		public void setNext(Node next) 
		{
			this.next = next;
		}	

	}

	public Node getNode(Key key)
	{
		int i = hash(key);
		

		return st[i] ;
	}

	public Value get(Key key) 
	{
		int i = hash(key);
		for (Node x = st[i]; x != null; x = x.getNext())
		{
			if (key.equals((Key)x.getKey())) 
				return (Value) x.getValue();
		}
		return null;	
	}

	public Node getNodeInt(int key)
	{
		return st[key] ;
	}
	
	public void put(Key key, Value val )
	{
		if (N/M > FactorDeCarga)
			resize(2*M); 
		
		int i = hash(key) ;
		if(st[i] ==  null)
		{
			st[i] = new Node(key, val) ;
		}
		else 
		{
			Node x = st[i] ;
			boolean t = true ;
			while(t)
			{
				if(x.getNext()!=null)
				{
					x = x.getNext() ;
				}
				else
					t = false;
				
			}
			x.setNext(new Node(key, val));
		}
		N++ ;
		
	}
	public void resize(int tamanio)
	{
		Node[] aux = new Node[tamanio];
		for(int i = 0; i < st.length ; i++)
		{
			aux[i] = st[i];
		}
		M = aux.length ;
		st = aux;
	}
	/*
	public void put(Key key, Value val)
	{
		int i = hash(key);
		for (Node x = st[i]; x != null; x = x.next)
		{	if (key.equals(x.getKey()))
			{
				x.setValue(val);
				return;
			}
		}
		st[i] = new Node(key, val);
	}
	 */

	public int getNumInAKey(Key k)
	{
		int i = hash(k) ;
		int num = 0 ;
		if(st[i]!=null)
		{
			Node x = st[i] ;
			while(x!=null)
			{
				num++ ;
				x = x.getNext() ;
			}
			
		}	
		
		return num;
		
	}
	
	public int size() 
	{
		// TODO Auto-generated method stub
		return M;
	}
	
	
	public void putServicioRango(int key, Value val )
	{		
		int i = (int) key/60 ;
		
		if(i>=M)
		{
			resize(2*i);
		}
		
		if(st[i] ==  null)
		{
			st[i] = new Node(key, val) ;
		}
		else 
		{
			Node x = st[i] ;
			boolean t = true ;
			while(t)
			{
				if(x.getNext()!=null)
				{
					x = x.getNext() ;
				}
				else
					t = false;
				
			}
			x.setNext(new Node(key, val));
		}
		N++ ;
		
	}
	
	

}
