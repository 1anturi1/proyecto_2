package model.data_structures;

public class Nodo <Key , Value>
{
	private Key Key;

	private Value Val;

	private Nodo Izq;

	private Nodo Der;

	private boolean Color;
	


	public Nodo (Key key, Value value, boolean color ) 
	{ 
		this.Key = key;
		this.Val = value;
		this.Color=color;
		this.Izq=null;
		this.Der=null;
	} 

	public Nodo getIzq()
	{
		return Izq;
	}
	public Nodo getDer()
	{
		return Der;
	}
	public Value getVal()
	{
		return Val;
	}
	public boolean color()
	{
		return Color;
	}
	public int size()
	{
		int num = 1 ;
		int numder = 0 ;
		int numiz = 0 ;

		if(Der!=null)
		{
			numder = Der.size() ;
		}
		
		if(Izq != null )
		{
			numiz = Izq.size() ;
		}
		
		if(numder > numiz)
		{
			num += numder ;
		}
		else
			num += numiz ;
			
		return num;
	}

	public void setKey(Key key)
	{
		Key = key;
	}
	public Key getKey() 
	{
		return Key; 
	}

	public void setVal(Value val) 
	{
		Val = val;
	}
	public void cambiarIzq(Nodo x)
	{
		Izq=x;
	}
	public void cambiarDer(Nodo x)
	{
		Der=x;
	}

	public void CambiarColor(Boolean a)
	{
		Color=a;
	}

}
