package VO;

import model.data_structures.LinkedList;
import model.data_structures.Lista;
import VO.Taxi;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private Lista<Taxi> taxisInscritos;	
	

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public Lista<Taxi> getTaxisInscritos()
	{
		return taxisInscritos;
	}

	public void setTaxisInscritos(Lista<Taxi> taxisInscritos) 
	{
		this.taxisInscritos = taxisInscritos;
	}

	@Override
	public int compareTo(Compania o) 
	{

		int aux = 0;
		if(nombre.compareTo(o.nombre)>0)
		{
			aux = 1;
		}
		else if(nombre.compareTo(o.nombre)<0)
		{
			aux = -1;
		}
		return aux;

	}
	
	
	

}
