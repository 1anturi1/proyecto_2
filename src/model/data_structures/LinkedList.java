package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T  
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>> 
{
	
	public void add(T bag ) ;
	
	public void delete( T bag) ;
	
	public T  getElement( T bag) ;
	
	public int size () ;
	
	public T get(T bag) ;
	//busca el T en la lista y retorna el elemento 
	public void listing() ;
	//posiciona en el primer nodo de la lista 
	public T getCurrent() ;
	//retorna el elemento t de ese recorrido, el actual 
	
	public boolean next() ;
	
	public void avanzar() ;
	
	public void combinarLista(Lista<T> pLista) ;
	
	public Node<T> getCabeza() ;
	
	
	

}