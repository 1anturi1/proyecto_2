package model.Logic;

import VO.Taxi;
import VO.TaxiConPuntos;
import VO.TaxiConServicios;
import VO.ZonaServicios;
import api.ITaxiTripsManager;
import model.data_structures.Lista;
import model.data_structures.Nodo;
import model.data_structures.Pila;
import model.data_structures.SeparateChainingHash;
import model.data_structures.SeparateChainingHash.Node;
import model.data_structures.Value;
import model.data_structures.arbolRojoNegro;
import sun.dc.pr.Rasterizer;
import model.data_structures.Cola;
import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedList;
import VO.Compania;
import VO.CompaniaServicios;
import VO.CompaniaTaxi;
import VO.InfoTaxiRango;
import VO.RangoDistancia;
import VO.RangoFechaHora;
import VO.Service;
import VO.Servicio;
import VO.ServiciosValorPagado;

import java.awt.List;
import java.awt.Point;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.sun.glass.ui.Pixels.Format;
import com.sun.org.apache.xml.internal.serialize.LineSeparator;

public class TaxiTripsManager implements ITaxiTripsManager 
{

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-0";

	private static arbolRojoNegro<String, Lista<Service>> arbolTaxis;

	private static arbolRojoNegro<String , Lista<Service>> arbolDistancia;	

	private static arbolRojoNegro<String, Lista<TaxiConServicios>> arbolCompa ;

	private static Lista<TaxiConPuntos> puntos;

	private static LinearProbingHashST<String, arbolRojoNegro<String, Lista<Service>>> tablaArbol;

	private static SeparateChainingHash<Integer, Service> Tabla2A ;

	private static arbolRojoNegro<String,Lista<Service>> arbol3C ;

	public void inicializarArbol()
	{
		puntos = new Lista<TaxiConPuntos>();
		arbolDistancia = new arbolRojoNegro<String, Lista<Service>>();
		arbolCompa = new arbolRojoNegro<String, Lista<TaxiConServicios>>();	
		tablaArbol= new LinearProbingHashST<>(30);
		Tabla2A = new SeparateChainingHash<>() ;

		arbol3C = new arbolRojoNegro<String, Lista<Service>>() ;
		arbolTaxis = new arbolRojoNegro<String,Lista<Service>>();
	}

	@Override
	public boolean cargarSistema(String direccionJson) 
	{

		JsonParser parser = new JsonParser();

		inicializarArbol();
		try 
		{
			String taxiTripsDatos = direccionJson;

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */

			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{

				JsonObject obj= (JsonObject) arr.get(i);
				//------------------------------------------
				String tripId = "";

				if ( obj.get("trip_id") != null )
				{ 
					tripId = obj.get("trip_id").getAsString(); 
				}
				//------------------------------------------
				String taxiId = "";
				if ( obj.get("taxi_id") != null )
				{ 
					taxiId = obj.get("taxi_id").getAsString(); 
				}
				//------------------------------------------
				String fechaini = "Nan";
				String horaini = "NaN" ;

				if ( obj.get("trip_start_timestamp") != null )
				{ 
					String a = (obj.get("trip_start_timestamp").getAsString());

					String [] b = a.split("T");
					fechaini = b[0] ;
					horaini = b[1] ;
				}

				//------------------------------------------
				String fechafin ="NaN" ;
				String horafin	="NaN" ;	

				if ( obj.get("trip_end_timestamp") != null )
				{ 
					String a = (obj.get("trip_end_timestamp").getAsString());
					String [] b = a.split("T");
					fechafin = b[0] ;
					horafin = b[1] ;

				}
				//------------------------------------------
				int tripSeconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ 
					tripSeconds = obj.get("trip_seconds").getAsInt(); 
				}
				//------------------------------------------
				double tripMiles = 0 ;	
				if ( obj.get("trip_miles") != null )
				{ 
					tripMiles = obj.get("trip_miles").getAsDouble();
				}

				//------------------------------------------
				String pickupCensusTrack = "" ;				
				if ( obj.get("pickup_census_tract") != null )
				{ 
					pickupCensusTrack = obj.get("pickup_census_tract").getAsString(); 
				}
				//------------------------------------------
				String dropoffCensusTract = "" ;				
				if ( obj.get("dropoff_census_tract") != null )
				{ 
					dropoffCensusTract = obj.get("dropoff_census_tract").getAsString(); 
				}
				//------------------------------------------
				int pickupCommunityArea = 0;
				if ( obj.get("pickup_community_area") != null )
				{ 
					pickupCommunityArea = obj.get("pickup_community_area").getAsInt(); 
				}
				//------------------------------------------
				int dropoffCommunityArea = 0;
				if ( obj.get("dropoff_community_area") != null )
				{ 
					dropoffCommunityArea = obj.get("dropoff_community_area").getAsInt(); 
				}
				//------------------------------------------
				float fare = 0;
				if ( obj.get("fare") != null )
				{ 
					fare = obj.get("fare").getAsFloat(); 
				}
				//------------------------------------------
				float tips = 0;
				if ( obj.get("tips") != null )
				{ 
					tips = obj.get("tips").getAsFloat(); 
				}

				//------------------------------------------
				float tolls = 0;
				if ( obj.get("tolls") != null )
				{ 
					tolls = obj.get("tolls").getAsFloat(); 
				}
				//------------------------------------------

				double extras = 0;
				if ( obj.get("extras") != null )
				{ 
					extras = obj.get("extras").getAsDouble(); 
				}
				//------------------------------------------
				float tripTotal =0 ;
				if ( obj.get("trip_total") != null )
				{ 
					tripTotal = obj.get("trip_total").getAsFloat(); 
				}

				//------------------------------------------
				String paymentType = "" ;				
				if ( obj.get("payment_type") != null )
				{ 
					paymentType = obj.get("payment_type").getAsString(); 
				}
				//------------------------------------------
				String company = "Independent Owner";
				if ( obj.get("company") != null )
				{ 
					company = obj.get("company").getAsString(); 
				}
				//------------------------------------------
				double pickupCentroidLatitude =0 ;
				if ( obj.get("pickup_centroid_latitude") != null )
				{ 
					pickupCentroidLatitude = obj.get("pickup_centroid_latitude").getAsDouble(); 
				}
				//------------------------------------------
				double pickupCentroidLongitude =0 ;
				if ( obj.get("pickup_centroid_longitude") != null )
				{ 
					pickupCentroidLongitude = obj.get("pickup_centroid_longitude").getAsDouble(); 
				}
				//------------------------------------------
				Point pickupCentroidLocation = null;		

				if ( obj.get("pickup_centroid_location") != null )
				{ 
					JsonObject a = obj.get("pickup_centroid_location").getAsJsonObject();
					JsonArray b = a.get("coordinates").getAsJsonArray();
					int x = b.get(0).getAsInt();
					int y = b.get(1).getAsInt();

					pickupCentroidLocation = new Point(x, y) ;

				}
				//------------------------------------------
				double dropoffCentroidLatitude =0 ;
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ 
					dropoffCentroidLatitude = obj.get("dropoff_centroid_latitude").getAsDouble(); 
				}
				//------------------------------------------
				double dropoffCentroidLongitude =0 ;
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ 
					dropoffCentroidLongitude = obj.get("dropoff_centroid_longitude").getAsDouble(); 
				}
				//------------------------------------------
				Point dropoffCentroidLocation = null ;
				if ( obj.get("dropoff_centroid_location") != null )
				{ 

					JsonObject a = obj.get("dropoff_centroid_location").getAsJsonObject(); 
					JsonArray b = a.get("coordinates").getAsJsonArray();
					int x = b.get(0).getAsInt();
					int y = b.get(1).getAsInt();

					dropoffCentroidLocation = new Point(x, y) ;

				}


				//--------------------------------------------

				Service ser = new Service(tripId, taxiId, fechaini, horaini, fechafin, horafin,
						tripSeconds, tripMiles, pickupCensusTrack, dropoffCensusTract,
						pickupCommunityArea, dropoffCommunityArea,
						fare, tips, tolls, extras, tripTotal, paymentType, company,
						pickupCentroidLatitude, pickupCentroidLongitude, pickupCentroidLocation, 
						dropoffCentroidLongitude,dropoffCentroidLatitude, dropoffCentroidLocation);



				//-----CARGA DE ARBOL ROJO-NEGRO CON LISTA DE TAXIS CON TABLAS DE HASH---------------------------------------------

				TaxiConServicios tax = new TaxiConServicios(taxiId, company);	

				//--------------------------------------------------

				if(arbolCompa.get(company)== null)
				{

					Lista<TaxiConServicios> aux = new Lista<TaxiConServicios>() ;
					tax.agregarServicio(ser);
					aux.add(tax);
					arbolCompa.put(company, aux);
				}
				else
				{

					if(arbolCompa.get(company).get(tax)==null)
					{
						arbolCompa.get(company).add(tax);
						arbolCompa.get(company).get(tax).agregarServicio(ser);


					}
					else
					{
						arbolCompa.get(company).get(tax).agregarServicio(ser);
					}

				}
				//-----CARGA DE TABLA DE HASH DE RANGOS--------------------------------------------

				Tabla2A.putServicioRango(tripSeconds, ser);	

				//------------------------------------------------------------------------------------

				//-----CARGA DE ARBOL ROJO-NEGRO CON DISTACNIAS---------------------------------------------


				String llaveDis =String.valueOf(tripMiles);
				if(arbolDistancia.get(llaveDis)==null)
				{
					Lista<Service> aux = new Lista<Service>();
					aux.add(ser);
					arbolDistancia.put(llaveDis, aux);
				}
				else
				{
					if(arbolDistancia.get(llaveDis).get(ser)==null)
					{
						arbolDistancia.get(llaveDis).add(ser);
					}
				}
				String rango= pickupCommunityArea+"-"+dropoffCommunityArea;
				//				System.out.println(rango);
				//				System.out.println("----------------");


				//-----CARGA DE TABLA DE HASH CON ARBOL ROJO-NEGRO---------------------------------------------


				if(tablaArbol.get(rango)==null)
				{
					arbolRojoNegro<String, Lista<Service>> aux= new arbolRojoNegro<String, Lista<Service>>();
					Lista<Service> aux2= new Lista<Service>();
					aux2.add(ser);
					aux.put(fechaini, aux2);
					tablaArbol.put(rango, aux);
				}
				else
				{
					arbolRojoNegro<String, Lista<Service>> r = (arbolRojoNegro<String, Lista<Service>>) tablaArbol.get(rango);
					if(r.get(fechaini)==null)
					{
						Lista<Service> pLista = new Lista<Service>();
						pLista.add(ser);
						arbolRojoNegro<String, Lista<Service>> arb = new arbolRojoNegro<>();
						arb.put(rango, pLista);
					}
					else
					{
						Lista<Service> t= r.get(fechaini);
						t.add(ser);		
					}
				}
				//---------------------------------------------CARGA PUNTO---------------------------------------------------------------

				TaxiConPuntos taxPuntos = new TaxiConPuntos(taxiId, company);


				if(puntos.get(taxPuntos)==null)
				{
					taxPuntos.agregar(ser);
					puntos.add(taxPuntos);
				}
				else
				{
					TaxiConPuntos o = puntos.get(taxPuntos);

					if(o.get(ser)==null)
					{
						o.agregar(ser);
					}
				}



				//----------------------------------CARGA ARBOL 3C--------------------------------------------------------------

				String llave = fechaini +" "+ horaini ;
				if(arbol3C.get(llave)==null)
				{
					Lista<Service> lista3= new Lista<Service>();
					lista3.add(ser);
					arbol3C.put(llave, lista3);	

				}
				else
				{
					arbol3C.get(llave).add(ser);
				}

				//----------------------------------------ARBOL TAXIS-----------------------------------------------------------------------------------------
				if(arbolTaxis.get(taxiId)==null)
				{
					Lista<Service> add = new Lista<Service>();
					add.add(ser);
					arbolTaxis.put(taxiId, add);
				}
				else
				{
					Lista<Service> e = new Lista<Service>();
					if(e.get(ser)==null)
					{
						e.add(ser);
					}
				}


				//				-------------------AQUI TERMINA LA CARGA------------------------------------------------

			}

			System.out.println(arbolCompa.getRaiz().getKey());


		}

		catch (JsonIOException e1 ) 
		{
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{


			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{

			e3.printStackTrace();
		}


		System.out.println(arbolCompa.size());
		return false ;
	}



	//Parte A
	@Override
	public Lista<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) 
	{
		Lista<TaxiConServicios> Taxis = arbolCompa.get(compania) ;
		Lista<TaxiConServicios> nueva = new Lista<TaxiConServicios>() ;
		int number = zonaInicio;

		Taxis.listing();
		int num = 0 ;

		while(Taxis.getActual()!=null)
		{

			TaxiConServicios actual = (TaxiConServicios) Taxis.getCurrent() ;
			if(actual.numServiciosEnArea(number) > num)
			{
				num = actual.numServiciosEnArea(number) ;
				nueva = new Lista<TaxiConServicios>() ;
				nueva.add(actual);

			}
			else if(actual.numServiciosEnArea(number)== num)
			{
				nueva.add(actual);

			}
			Taxis.avanzar();

		}



		return nueva;

	}


	@Override
	public IList<Service> A2ServiciosPorDuracion(int duracion) 
	{

		Lista<Service> aux = new Lista<Service>() ;
		int i = (int) duracion/60 ;


		Node actual = Tabla2A.getNode(i) ;
		while(actual!=null)
		{
			aux.add((Service) actual.getValue());
			actual = actual.getNext() ;
		}

		return aux;
	}

	//parte B
	@Override
	public Lista<Service> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) 
	{
		// TODO Auto-generated method stub
		Lista<Service> aux= new Lista<Service>();

		String minima =String.valueOf(distanciaMinima);
		String maxima =String.valueOf(distanciaMaxima);

		Lista<Service> retornar= new Lista<Service>();
		b1(minima, maxima, arbolDistancia.getRaiz(), aux);
		aux.listing();
		while(true)
		{
			Service add = aux.getCurrent();
			if(add.getTripMiles()<=distanciaMaxima)
			{
				if(add.getTripMiles()>=distanciaMinima)
				{
					retornar.add(add);
				}
			}
			if(aux.next())
			{
				aux.avanzar();
			}
			else
			{
				break;
			}
		}

		return retornar;
	}
	public void b1(String minima, String maxima, Nodo<String, Lista<Service>> extra,Lista<Service> retorn )
	{
		if(extra!=null)
		{
			if (extra.getKey().compareTo(minima)>=0)
			{ 

				if(maxima.compareTo(extra.getKey())>=0)
				{

					Lista<Service> aux= extra.getVal();
					aux.listing();
					while(true)
					{
						Service a = aux.getCurrent();
						retorn.add(a);
						if(aux.next())
						{
							aux.avanzar();
						}
						else
						{
							break;
						}
					}			

				}

			}
			b1(minima, maxima, extra.getDer(), retorn);
			b1(minima, maxima, extra.getIzq(), retorn);
		}

	}
	@Override
	public Lista<Service> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI,
			String fechaF, String horaI, String horaF) 
	{
		Lista<Service> retornar= new Lista<Service>();
		Lista<Service> aux2 = new Lista<Service>();
		arbolRojoNegro<String, Lista<Service>> aux=(arbolRojoNegro<String, Lista<Service>>) tablaArbol.get(zonaInicio+"-"+zonaFinal);
		if(aux==null)
		{
			aux2=null;
		}
		else
		{
			b1(fechaI,fechaF,aux.getRaiz(),retornar);

			retornar.listing();
			while(true)
			{
				Service add= retornar.getCurrent();

				if(add.getHoraInicial().compareTo(horaI)>=0)
				{
					if(add.getHoraFinal().compareTo(fechaF)<=0)
					{
						aux2.add(add);
					}
				}

				if(retornar.next())
				{
					retornar.avanzar();
				}
				else
				{
					break;
				}
			}
		}
		return aux2;
	}
	public TaxiConPuntos darMayor(Lista<TaxiConPuntos> pLista)
	{

		pLista.listing();
		TaxiConPuntos aux=pLista.getCurrent();
		while(true)
		{
			TaxiConPuntos d = pLista.getCurrent();
			if(d.getPuntos()>aux.getPuntos())
			{
				aux=d;
			}

			if(pLista.next())
			{			
				pLista.avanzar();
			}
			else
			{
				break;
			}
		}
		return aux;
	}

	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {

		TaxiConPuntos[] retornar = new TaxiConPuntos[puntos.size()];
		Lista<TaxiConPuntos> r = puntos;
		int cantidad=puntos.size();
		for(int i =0; i<cantidad;i++)
		{
			TaxiConPuntos eli=darMayor(r);
			retornar[i] = eli;
			r.delete(eli);

		}
		return retornar;
	}
	@Override
	public Lista<Service> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millas,double latitud,double longitud)
	{
		Lista<Service> r = arbolTaxis.get(taxiIDReq2C);
		SeparateChainingHash<String, Lista<Service>> retornar = new SeparateChainingHash<String, Lista<Service>>();
		r.listing();
		//		id: 7ed122481c0964a5555309bf4696e25bbf7def086d7ecb94b4910b6129501468f748621f8c51a9e0daaaa8ecca9624925356a53e2860cefdbc3caf730404d9a9
		//		latitud 41.884987192
		//		longitud -87.620992913
		//		
		String f = String.valueOf(millas);
		if(r!=null)
		{
			while(true)
			{
				Service proc = r.getCurrent();

				double d =getDistance(latitud, longitud, proc.getPickupCentroidLatitude(), proc.getPickupCentroidLongitude());

				double b =d*10; 
				int a = (int)b;
				double c = (a*1.0)/10;
				String llave = String.valueOf(c);

				if(retornar.get(llave)==null)
				{
					Lista<Service> add = new Lista<Service>();
					add.add(proc);
					retornar.put(llave, add);
				}
				else
				{
					Lista<Service> t = retornar.get(llave);
					if(t.get(proc)==null)
					{
						t.add(proc);
					}
				}

				if(r.next())
				{
					r.avanzar();
				}
				else
				{
					break;
				}
			}
		}
		Lista<Service> qwe = retornar.get(f);

		return qwe;
	}
	private double toRad(double ele)
	{
		return ele*0.0174533;
	}

	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final int R = 3959;
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))
		* Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}
	@Override
	public IList<Service> R3C_ServiciosEn15Minutos(String fecha, String hora) 
	{

		String[]fechas = fecha.split("-") ;
		String a�o = fechas[0] ;
		String mes = fechas[1] ;
		int dias = Integer.parseInt(fechas[2]) ;

		String[] numeros = hora.split(":") ;
		int horas = Integer.parseInt(numeros[0]) ;
		double minutos1 = Double.parseDouble(numeros[1]) ;
		String segundos = numeros[2] ;
		minutos1 = (Math.round(minutos1/15))*15 ;

		int minutos = (int) minutos1 ;

		if(minutos>=60)
		{
			minutos = 00;
			horas++ ;
		}
		if(horas>23)
		{
			horas = 00 ;
			dias++ ;
		}

		String diasLetra = Integer.toString(dias) ;
		if(dias>=0 && dias < 10)
		{
			diasLetra="0"+diasLetra ;
		}


		String horasLetra =Integer.toString(horas) ;
		if(horas>=0 && horas < 10)
		{
			horasLetra="0"+horasLetra ;
		}

		String minutosLetra = Integer.toString(minutos);
		if(minutos>=0 && minutos < 10)
		{
			minutosLetra="0"+minutosLetra ;
		}


		String cadena = a�o+"-" + mes+"-"+diasLetra+" "+horasLetra+":"+minutosLetra+":"+ segundos ;
		System.out.println(cadena);

		Lista<Service> aux = arbol3C.get(cadena) ;
		Lista<Service> nueva = new Lista<Service>() ;

		aux.listing();
		while(aux.getActual()!=null)
		{
			Service actual = aux.getCurrent() ;

			if(actual.getPickupCommunityArea()!= actual.getDropoffCommunityArea())
			{
				nueva.add(actual);
			}
			aux.avanzar();

		}



		return nueva ;

	}
}
