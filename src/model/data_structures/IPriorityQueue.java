package model.data_structures;

public interface IPriorityQueue <T>
{

	
	public void insert(T pInfo) ;
	
	public T remove() ;
	
	public void swim(int k) ;
	
	public void sink(int k) ;
	
	public boolean less (int i, int j) ;
	
	public void exch(int i, int j) ;
	
	public boolean isEmpty();
	
	public int size() ;
	
	
}
