package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import VO.Taxi;

public class TaxiTest
{

	private Taxi taxi;


	@Before
	public void setUpEscenario1()
	{
		taxi= new Taxi("radio taxi internacional", "abcd123");
	}

	@Test
	public void testTaxi()
	{
		assertTrue("La compa�ia no es correcta", taxi.getCompany().equals("radio taxi internacional"));
		assertTrue("El id del taxi no es correcto", taxi.getTaxiId().equals("abc123"));
	}

	@Test
	public void testCompare()
	{
		//caso1
		Taxi taxi1= new Taxi("rtc", "acdfer");
		assertEquals("El resultado de comparacion no es correcto", taxi.compareTo(taxi1),-1);
		//caso 2
		Taxi taxi2=new Taxi("radio taxi internacional", "abcd123");
		assertEquals("El resultado de comparacion no es correcto", taxi.compareTo(taxi2),0);
		//caso3
		Taxi taxi3=new Taxi("radio taxi internacional", "abcd1");
		assertEquals("El resultado de comparacion no es correcto", taxi.compareTo(taxi3),1);
	}
}


