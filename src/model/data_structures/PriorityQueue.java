package model.data_structures;

import java.sql.Array;

public class PriorityQueue <T extends Comparable <T>> implements IPriorityQueue<T>
{

	private int N ;
	private T[] arreglo ;
	
	public PriorityQueue(int i)
	{
		arreglo = (T[]) new Comparable[i +1] ;
		N = 0 ;
	}
	
	
	@Override
	public void insert(T pInfo)
	{
		arreglo[++N] = pInfo;
		swim(N);
		
	}


	@Override
	public T remove() 
	{
		
		T max = arreglo[1]; // Retrieve max key from top.
		exch(1, N--); // Exchange with last item.
		arreglo[N+1] = null; // Avoid loitering.
		sink(1); // Restore heap property.
		return max;
		
	}
	
	
	public T[] getArreglo() 
	{
		return arreglo;
	}


	@Override
	public void swim(int k) 
	{
		while (k > 1 && less(k/2, k))
		{
		exch(k/2, k);
		k = k/2;
		}		
	}

	@Override
	public void sink(int k)
	{
		while (2*k <= arreglo.length)
		{
		int j = 2*k;
		if (j < arreglo.length && less(j, j+1)) j++;
		if (!less(k, j)) break;
		exch(k, j);
		k = j;
		}
	}

	@Override
	public boolean less(int i, int j) 
	{
		
		return arreglo[i].compareTo(arreglo[j]) < 0;
	}

	@Override
	public void exch(int i, int j) 
	{
		T t = arreglo[i]; 
		arreglo[i] = arreglo[j]; 
		arreglo[j] = t;		
	}

	@Override
	public boolean isEmpty() 
	{
		return arreglo.length == 0?true :false ; 
	}

	@Override
	public int size() 
	{
		return arreglo.length;
	}

	
	

}
