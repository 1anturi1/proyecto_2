package model.data_structures;

import java.util.Iterator;

import VO.Taxi;

public class Lista<T extends Comparable <T>> implements IList<T>
{
	private Node<T> cabeza ;

	private Node<T> actual ;

	private Node<T> ultimo ;

	private int size ;

	public Lista()
	{
		cabeza = null ;

		actual= cabeza ;

		ultimo = cabeza ;

		size = 0 ;

	}
	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(T elem)
	{
		// TODO Auto-generated method stub
		if(cabeza == null )
		{
			cabeza = new Node<T>(elem) ;
			ultimo = cabeza ;
		}
		else 
		{
			Node<T> nuevo = new Node<T>(elem) ;
			ultimo.cambiarSig(nuevo);
			ultimo = nuevo ;

		}
		size++;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size ;
	}
	public void listing() 
	{
		actual = cabeza ;

	}
	public boolean next() 
	{
		return actual.darSig() != null? true : false;

	}

	public void avanzar()
	{
		actual = actual.darSig() ;
	}


	public Node<T> getUltimo() 
	{
		return ultimo;
	}

	@Override
	public T get(T elem)
	{
		T aux = null;
		// TODO Auto-generated method stub
		listing();
		while(actual != null)
		{
			if(elem.compareTo(getCurrent())==0)
			{
				aux = getCurrent();
				break ;
			}
			else
			{
				avanzar();
			}
		}
		return aux;
	}
	
	
	public void delete(T bag) 
	{
		if(bag!=null)
		{
			if(cabeza != null )
			{
				boolean vari = true ;
				if(cabeza.darT().compareTo(bag)==0)
				{
					cabeza = cabeza.darSig() ;
					size-- ;
				}
				else if(ultimo.darT().compareTo(bag)==0)
				{	
			
				listing();
				while(true)
				{
					if(actual.darSig().darT().compareTo(bag)==0)
					{
						
						ultimo=actual;
						ultimo.cambiarSig(null);
			
						break;
					}
					else
					{
						avanzar();
					}
					}
				}
				else
				{
					listing();
					while (vari)
					{

						if(actual.darSig().darT().equals(bag))
						{
							actual.cambiarSig(actual.darSig().darSig());
							size-- ;
							vari = false;
						}
						else if(actual == null)
						{
							vari = false ;
						}
						else
							avanzar();

					}
				}

			}
		}
	}	public T getCurrent() 
	{
		return actual.darT();
	}
	public Node<T> getActual()
	{
		return actual ;
	}

	public void combinarLista(Lista<T> pLista)
	{
		ultimo.cambiarSig(pLista.getCabeza());
	}

	public Node<T> getCabeza()
	{
		return cabeza ;
	}


}
