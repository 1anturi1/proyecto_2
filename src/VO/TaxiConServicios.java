package VO;

import model.data_structures.IList;
import model.data_structures.LinearProbingHashST;
import model.data_structures.Lista;
import model.data_structures.SeparateChainingHash;
import model.data_structures.SeparateChainingHash.Node;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

	private String taxiId;
	private String compania;
	private SeparateChainingHash<String,Service> servicios;
	

	public TaxiConServicios(String taxiId, String compania)
	{
		this.taxiId = taxiId;
		this.compania = compania;
	
		servicios = new SeparateChainingHash<String, Service>();
	}

	public String getTaxiId() 
	{
		return taxiId;
	}

	public String getCompania() 
	{
		return compania;
	}

	public SeparateChainingHash<String, Service> getServicios()
	{
		return servicios;
	}

	public int numeroServicios()
	{
		return servicios.size();
	}
	
	
	public int numServiciosEnArea(int num)
	{
		String key = Integer.toString(num) ;		
		return servicios.getNumInAKey(key) ;
	}

	public void agregarServicio(Service servicio)
	{
		String z = Integer.toString(servicio.getCommunityArea()) ; 	
		servicios.put(z,servicio );
	}

	@Override
	public int compareTo(TaxiConServicios o) 
	{
		return taxiId.compareTo( o.getTaxiId());
	}

	public void print()
	{
		System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);

		for(int i = 0; i < servicios.size() ; i ++)
		{
			String z = Integer.toString(i) ; 	
			Service actual = (Service) servicios.get(z) ;
			
			System.out.println("\t"+actual.getFechaInicial());
		}

		System.out.println("___________________________________");;
	}
	
}
