package VO;

import model.data_structures.LinkedList;

import model.data_structures.Lista;

import VO.Servicio;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private Lista<Service> servicios;
	
	
	public CompaniaServicios(String pNombre, Lista<Service> pLista )
	{
		nomCompania = pNombre ;
		servicios = pLista ;
	}
	

	public String getNomCompania() 
	{
		return nomCompania;
	}


	public Lista<Service> getServicios() 
	{
		return servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o)
	{
		return nomCompania.compareTo(o.getNomCompania()) ;
	}
}
